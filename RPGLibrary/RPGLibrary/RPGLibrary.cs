﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public abstract class RPGBaseClass
    {
        // Yes, this follows the DnD 5e class system

        protected string baseClassName;
        protected string subClassName;
        protected string resourceName;

        private int hp;
        private int resource;
        private string name;
        private int armorRating;

        public int Hp { get => hp; set => hp = value; }
        public int Resource { get => resource; set => resource = value; }
        public string Name { get => name; set => name = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }
        public string ResourceName { get => resourceName; }
        public string SubClassName { get => subClassName; }
        public string BaseClassName { get => baseClassName; }

        public virtual string Attack()
        {
            return "Generic attack";
        }

        public virtual string Move()
        {
            return "Moved";
        }

        public virtual Dictionary<string, string> GetClassInfo()
        {
            return new Dictionary<string, string>
            {
                { "Name" , Name },
                { "Base Class" , baseClassName },
                { "Sub Class" , subClassName },
                { "HP" , Hp.ToString() },
                { "Resource Name" , resourceName },
                { "Resource" , Resource.ToString() },
                { "Armor Rating" , ArmorRating.ToString() },
            };
        }

        public virtual bool SetSchool(string school) { return false; }

    }
}

