﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Eldritch_Knight : Fighter
    {
        public Eldritch_Knight(string name)
        {
            Name = name;
            baseClassName = "Fighter";
            Hp = 90;
            resourceName = "Mana";
            Resource = 100;
            ArmorRating = 15;
            subClassName = "Eldritch Knight";
        }
    }
}
