﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Rogue : RPGBaseClass
    {
        List<string> subClasses = new List<string>
        {
            "Thief",
            "Assassin",
            "Arcane Trickster"
        };

        public Rogue()
        {
            Name = "Bob";
            baseClassName = "Rogue";
            Hp = 70;
            resourceName = "Energy";
            Resource = 100;
            ArmorRating = 14;
            subClassName = "";
        }

        public Rogue(string name)
        {
            Name = name;
            baseClassName = "Rogue";
            Hp = 70;
            resourceName = "Energy";
            Resource = 100;
            ArmorRating = 14;
            subClassName = "";
        }

        public List<string> GetSubClasses()
        {
            return subClasses;
        }

    }
}
