﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Champion : Fighter
    {
        public Champion(string name)
        {
            Name = name;
            baseClassName = "Fighter";
            Hp = 130;
            resourceName = "Rage";
            Resource = 0;
            ArmorRating = 16;
            subClassName = "Champion";
        }

    }
}
