﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Fighter : RPGBaseClass
    {
        List<string> subClasses = new List<string>
        {
            "Champion",
            "Battle Master",
            "Eldritch Knight"
        };
        public Fighter()
        {
            Name = "Bob";
            baseClassName = "Fighter";
            Hp = 100;
            resourceName = "Rage";
            Resource = 0;
            ArmorRating = 16;
            subClassName = "";
        }

        public Fighter(string name)
        {
            Name = name;
            baseClassName = "Fighter";
            Hp = 100;
            resourceName = "Rage";
            Resource = 0;
            ArmorRating = 16;
            subClassName = "";
        }

        public List<string> GetSubClasses()
        {
            return subClasses;
        }
    }
}
