﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Battle_Master : Fighter
    {
        public Battle_Master(string name)
        {
            Name = name;
            baseClassName = "Fighter";
            Hp = 110;
            resourceName = "Rage";
            Resource = 20;
            ArmorRating = 16;
            subClassName = "Battle Master";
        }
    }
}
