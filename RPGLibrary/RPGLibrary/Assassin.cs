﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Assassin : Rogue
    {
        public Assassin(string name)
        {
            Name = name;
            baseClassName = "Rogue";
            Hp = 75;
            resourceName = "Energy";
            Resource = 100;
            ArmorRating = 15;
            subClassName = "Assassin";
        }
    }
}
