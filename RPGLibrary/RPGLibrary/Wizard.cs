﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Wizard : RPGBaseClass
    {
        // Didn't want to create a new class per school of magic in this example since
        // the school of magic does not drastically change the class, might just add some new spells.
        List<string> SchoolsOfMagic = new List<string>
        {
            "Abjuration",
            "Conjuration",
            "Enchanting",
            "Divination",
            "Evocation",
            "Illusion",
            "Necromancy",
            "Transmutation"
        };

        public Wizard()
        {
            Name = "Bob";
            baseClassName = "Wizard";
            Hp = 50;
            resourceName = "Mana";
            Resource = 150;
            ArmorRating = 10;
            subClassName = "";
        }

        public Wizard(string name)
        {
            Name = name;
            baseClassName = "Wizard";
            Hp = 50;
            resourceName = "Mana";
            Resource = 150;
            ArmorRating = 10;
            subClassName = "";
        }

        public override bool SetSchool(string school)
        {
            if (SchoolsOfMagic.Contains(school))
            {
                subClassName = school;
                return true;
            }
            return false;
        }

        public List<string> GetSubClasses()
        {
            return SchoolsOfMagic;
        }

    }
}
