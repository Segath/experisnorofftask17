﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Thief : Rogue
    {
        public Thief(string name)
        {
            Name = name;
            baseClassName = "Rogue";
            Hp = 65;
            resourceName = "Energy";
            Resource = 110;
            ArmorRating = 15;
            subClassName = "Thief";
        }
    }
}
