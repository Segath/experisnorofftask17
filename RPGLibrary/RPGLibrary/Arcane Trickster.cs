﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGLibrary
{
    public class Arcane_Trickster : Rogue
    {
        public Arcane_Trickster(string name)
        {
            Name = name;
            baseClassName = "Rogue";
            Hp = 70;
            resourceName = "Mana";
            Resource = 110;
            ArmorRating = 15;
            subClassName = "Arcane Trickster";
        }

    }
}
