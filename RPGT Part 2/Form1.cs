﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGLibrary;
using System.Data.SQLite;

namespace RPGT_Part_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        //creating the fighter, rouge, wizard and character object
        Fighter fighter = new Fighter();
        Rogue rogue = new Rogue();
        Wizard wizard = new Wizard();
        RPGBaseClass character = null;

        #region Data Definition Operations
        static void CreateTable(SQLiteConnection conn)
        {
            //This command object is used to execute the transacations 
            SQLiteCommand sqlite_cmd;

            string Createsql = "CREATE TABLE SampleTable (Col1 VARCHAR(20), Col2 INT)";
            string Createsql1 = "CREATE TABLE SampleTable1 (Col1 VARCHAR(20), Col2 INT)";

            sqlite_cmd = conn.CreateCommand();

            sqlite_cmd.CommandText = Createsql;
            sqlite_cmd.ExecuteNonQuery();

            sqlite_cmd.CommandText = Createsql1;
            sqlite_cmd.ExecuteNonQuery();
        }

        #endregion

        #region Data Manipulations Operations
        static void InsertData(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;

            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "INSERT INTO SampleTable (Col1, Col2) VALUES('Test Text ', 1); ";
            sqlite_cmd.ExecuteNonQuery();
        }

        static void ReadData(SQLiteConnection conn)
        {
            SQLiteDataReader sqlite_datareader;

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM SampleTable";

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            while (sqlite_datareader.Read())
            {
                string myreader = sqlite_datareader.GetString(0) + sqlite_datareader.GetInt32(1);
                Console.WriteLine(myreader);
            }
        }
        #endregion

        #region writeout to Character text
        private void Button1_Click(object sender, EventArgs e)
        {
            //Writing the summary text to the Character file using streamwriter.
            string path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string text = "";

            using (StreamWriter sw = new StreamWriter("Character.txt"))
            {
                foreach (KeyValuePair<string, string> c in character.GetClassInfo())
                {
                    text += (c.Key + ":" + c.Value + " " + "\n");
                }
                sw.WriteLine(text);
                summary_Box.Text = text;
            }  
        }
        #endregion

        #region methods to set characters
        private void subclass_ComboBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //sets the default value after selecting the base and sublasses
            if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Champion"))
            {
                character = new Champion(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                resource_TextBox.Text = (character.Resource = 50).ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            }
            else if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Eldritch Knight"))
            {
                character = new Eldritch_Knight(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            }
            else if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Battle Master"))
            {
                character = new Battle_Master(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            }
            else if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Thief"))
            {
                character = new Thief(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            }
            else if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Assassin"))
            {
                character = new Assassin(name_Box.Text);
                hp_TextBox.Text = character.ArmorRating.ToString();
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.Resource.ToString();
            }
            else if (subclass_ComboBox.SelectedItem != null && subclass_ComboBox.SelectedItem.Equals("Arcane Trickster"))
            {
                character = new Arcane_Trickster(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            }
            else if (baseClass_box.SelectedItem != null && baseClass_box.SelectedItem.Equals("Wizard"))
            {
                character = new Wizard(name_Box.Text);
                hp_TextBox.Text = character.Hp.ToString();
                character.SetSchool(subclass_ComboBox.SelectedItem.ToString());
                resource_TextBox.Text = character.Resource.ToString();
                armour_TextBox.Text = character.ArmorRating.ToString();
            };
        }


        private void baseClass_box_SelectedIndexChanged(object sender, EventArgs e)
        {
            //after selecting the base class
            //for fighter
            if (baseClass_box.SelectedItem.Equals("Fighter"))
            {
                subclass_ComboBox.Items.Clear();   
                foreach (string s in fighter.GetSubClasses())
                {
                    subclass_ComboBox.Items.Add(s);
                }
            }
            //for wizard
            else if (baseClass_box.SelectedItem.Equals("Wizard"))
            {
                subclass_ComboBox.Items.Clear();
                foreach(string s in wizard.GetSubClasses())
                {
                    subclass_ComboBox.Items.Add(s);          
                }
            }
            //for rogue
            else if(baseClass_box.SelectedItem.Equals("Rogue")) {
                subclass_ComboBox.Items.Clear();
                foreach (string s in rogue.GetSubClasses()) {
                    subclass_ComboBox.Items.Add(s);
                }               
            }
        }

        private void hp_TextBox_TextChanged(object sender, EventArgs e)
        {

        }


        private void Summary_Click(object sender, EventArgs e)
        {

        }


        private void armour_TextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void Summary_Box_TextChanged(object sender, EventArgs e)
        {

        }
        #endregion

        //Creates the connection to a specified database
        static SQLiteConnection CreateConnection()
        {
            string projectDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            SQLiteConnection sqlite_conn;
            // Create a new database connection:

            sqlite_conn = new SQLiteConnection($"Data Source = {projectDir}/database/CharacterDatabase.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }

        #region Data Manipulations Operations
        private void InsertBtn_Click(object sender, EventArgs e)
        {
            //writeout to CharacterInsertSQL text
            string text = ""; 
            using (StreamWriter sw = new StreamWriter("CharacterInsertSQL.txt"))
            {

                sw.WriteLine($"INSERT INTO Character(Name, Base_Class, Subclass, HP, Resource, Armour_Rating) VALUES(" +
                    $"\"{character.Name}\", \"{character.BaseClassName}\", \"{character.SubClassName}\"" +
                    $", {character.Hp} , { character.Resource} , {character.ArmorRating});");
                // text += (c.Key + ": " + c.Value + ", " + "\n");

                sw.WriteLine(text);
            }
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;

            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"INSERT INTO Character (Name, Base_Class, Subclass, HP, Resource, Armour_Rating )" +
                $" VALUES(\"{character.Name}\", \"{character.BaseClassName}\", \"{character.SubClassName}\", " +
                $" {character.Hp} , {character.Resource} , {character.ArmorRating}); ";
            sqlite_cmd.ExecuteNonQuery();
            conn.Close();
        }

        //reading everything from the table 
        private void ReadBtn_Click(object sender, EventArgs e)
        {
            summary_Box.Clear(); 
            SQLiteConnection conn = CreateConnection();
            SQLiteDataReader sqlite_datareader;

            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = "SELECT * FROM Character"; 

            sqlite_datareader = sqlite_cmd.ExecuteReader();

            summary_Box.Text = "From database \n" ;
            while (sqlite_datareader.Read())
            {
                string myreader = sqlite_datareader["Name"].ToString() + " " + sqlite_datareader["Base_Class"].ToString() + " " + sqlite_datareader["Subclass"].ToString()
                    + " " + sqlite_datareader["HP"].ToString() + " " + sqlite_datareader["Resource"].ToString() + " " + sqlite_datareader["Armour_Rating"].ToString();
                summary_Box.Text += myreader+ "\n"; 
            }
        }
        #endregion

        #region Data Definition Operations

        #endregion

        #region delete and create operations
        private void DeleteBtn_Click(object sender, EventArgs e)
        {        
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"DELETE from Character WHERE Name = \"{textDeleteInput.Text}\";";
            sqlite_cmd.ExecuteNonQuery();
            conn.Close();

        }
        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            SQLiteConnection conn = CreateConnection();
            SQLiteCommand sqlite_cmd;
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = $"UPDATE Character SET Name = \"{textNewName.Text}\" WHERE Name = \"{textOldName.Text}\";";
            sqlite_cmd.ExecuteReader();
            conn.Close();
        }
        #endregion    
    }
}
