﻿namespace RPGT_Part_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.name_Box = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.hp_TextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.armour_TextBox = new System.Windows.Forms.TextBox();
            this.subclass_ComboBox = new System.Windows.Forms.ComboBox();
            this.baseClass_box = new System.Windows.Forms.ComboBox();
            this.summary = new System.Windows.Forms.Label();
            this.resource_TextBox = new System.Windows.Forms.TextBox();
            this.summary_Box = new System.Windows.Forms.RichTextBox();
            this.insertBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.readBtn = new System.Windows.Forms.Button();
            this.updateBtn = new System.Windows.Forms.Button();
            this.textDeleteInput = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textNewName = new System.Windows.Forms.TextBox();
            this.textOldName = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome!";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Choose the characters name, base class and subclass:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 387);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 40);
            this.button1.TabIndex = 11;
            this.button1.Text = "Create character";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // name_Box
            // 
            this.name_Box.AccessibleName = "name_Box";
            this.name_Box.Location = new System.Drawing.Point(160, 98);
            this.name_Box.Name = "name_Box";
            this.name_Box.Size = new System.Drawing.Size(154, 22);
            this.name_Box.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 15;
            this.label3.Text = "Name:";
            // 
            // hp_TextBox
            // 
            this.hp_TextBox.Location = new System.Drawing.Point(160, 227);
            this.hp_TextBox.Name = "hp_TextBox";
            this.hp_TextBox.Size = new System.Drawing.Size(154, 22);
            this.hp_TextBox.TabIndex = 18;
            this.hp_TextBox.TextChanged += new System.EventHandler(this.hp_TextBox_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 139);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 17);
            this.label4.TabIndex = 21;
            this.label4.Text = "Base class name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(30, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 22;
            this.label5.Text = "Subclass:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(30, 227);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 17);
            this.label6.TabIndex = 23;
            this.label6.Text = "HP:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(30, 271);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 17);
            this.label8.TabIndex = 25;
            this.label8.Text = "Resource:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(30, 320);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 17);
            this.label9.TabIndex = 27;
            this.label9.Text = "Armour rating:";
            // 
            // armour_TextBox
            // 
            this.armour_TextBox.Location = new System.Drawing.Point(160, 320);
            this.armour_TextBox.Name = "armour_TextBox";
            this.armour_TextBox.Size = new System.Drawing.Size(154, 22);
            this.armour_TextBox.TabIndex = 26;
            this.armour_TextBox.TextChanged += new System.EventHandler(this.armour_TextBox_TextChanged);
            // 
            // subclass_ComboBox
            // 
            this.subclass_ComboBox.FormattingEnabled = true;
            this.subclass_ComboBox.Location = new System.Drawing.Point(160, 185);
            this.subclass_ComboBox.Name = "subclass_ComboBox";
            this.subclass_ComboBox.Size = new System.Drawing.Size(154, 24);
            this.subclass_ComboBox.TabIndex = 28;
            this.subclass_ComboBox.SelectedIndexChanged += new System.EventHandler(this.subclass_ComboBox_SelectedIndexChanged_1);
            // 
            // baseClass_box
            // 
            this.baseClass_box.FormattingEnabled = true;
            this.baseClass_box.Items.AddRange(new object[] {
            "Fighter",
            "Wizard",
            "Rogue"});
            this.baseClass_box.Location = new System.Drawing.Point(160, 139);
            this.baseClass_box.Name = "baseClass_box";
            this.baseClass_box.Size = new System.Drawing.Size(154, 24);
            this.baseClass_box.TabIndex = 31;
            this.baseClass_box.SelectedIndexChanged += new System.EventHandler(this.baseClass_box_SelectedIndexChanged);
            // 
            // summary
            // 
            this.summary.AutoSize = true;
            this.summary.Location = new System.Drawing.Point(411, 102);
            this.summary.Name = "summary";
            this.summary.Size = new System.Drawing.Size(0, 17);
            this.summary.TabIndex = 33;
            this.summary.Click += new System.EventHandler(this.Summary_Click);
            // 
            // resource_TextBox
            // 
            this.resource_TextBox.Location = new System.Drawing.Point(160, 271);
            this.resource_TextBox.Name = "resource_TextBox";
            this.resource_TextBox.Size = new System.Drawing.Size(154, 22);
            this.resource_TextBox.TabIndex = 34;
            // 
            // summary_Box
            // 
            this.summary_Box.Location = new System.Drawing.Point(862, 22);
            this.summary_Box.Name = "summary_Box";
            this.summary_Box.Size = new System.Drawing.Size(270, 333);
            this.summary_Box.TabIndex = 36;
            this.summary_Box.Text = "";
            this.summary_Box.TextChanged += new System.EventHandler(this.Summary_Box_TextChanged);
            // 
            // insertBtn
            // 
            this.insertBtn.Location = new System.Drawing.Point(221, 387);
            this.insertBtn.Name = "insertBtn";
            this.insertBtn.Size = new System.Drawing.Size(238, 40);
            this.insertBtn.TabIndex = 37;
            this.insertBtn.Text = "Insert character to the database";
            this.insertBtn.UseVisualStyleBackColor = true;
            this.insertBtn.Click += new System.EventHandler(this.InsertBtn_Click);
            // 
            // deleteBtn
            // 
            this.deleteBtn.Location = new System.Drawing.Point(576, 123);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(151, 40);
            this.deleteBtn.TabIndex = 38;
            this.deleteBtn.Text = "Delete Character";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // readBtn
            // 
            this.readBtn.Location = new System.Drawing.Point(862, 387);
            this.readBtn.Name = "readBtn";
            this.readBtn.Size = new System.Drawing.Size(270, 46);
            this.readBtn.TabIndex = 40;
            this.readBtn.Text = "Read characters from the database";
            this.readBtn.UseVisualStyleBackColor = true;
            this.readBtn.Click += new System.EventHandler(this.ReadBtn_Click);
            // 
            // updateBtn
            // 
            this.updateBtn.Location = new System.Drawing.Point(576, 297);
            this.updateBtn.Name = "updateBtn";
            this.updateBtn.Size = new System.Drawing.Size(151, 40);
            this.updateBtn.TabIndex = 41;
            this.updateBtn.Text = "Update Database";
            this.updateBtn.UseVisualStyleBackColor = true;
            this.updateBtn.Click += new System.EventHandler(this.UpdateBtn_Click);
            // 
            // textDeleteInput
            // 
            this.textDeleteInput.Location = new System.Drawing.Point(576, 77);
            this.textDeleteInput.Name = "textDeleteInput";
            this.textDeleteInput.Size = new System.Drawing.Size(151, 22);
            this.textDeleteInput.TabIndex = 42;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(494, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(327, 17);
            this.label7.TabIndex = 43;
            this.label7.Text = "Write the name of the character you want to delete";
            // 
            // textNewName
            // 
            this.textNewName.Location = new System.Drawing.Point(685, 258);
            this.textNewName.Name = "textNewName";
            this.textNewName.Size = new System.Drawing.Size(151, 22);
            this.textNewName.TabIndex = 44;
            // 
            // textOldName
            // 
            this.textOldName.Location = new System.Drawing.Point(497, 258);
            this.textOldName.Name = "textOldName";
            this.textOldName.Size = new System.Drawing.Size(151, 22);
            this.textOldName.TabIndex = 45;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(478, 227);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(367, 17);
            this.label10.TabIndex = 46;
            this.label10.Text = "Write the old name to the left, and the new on to the right";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1144, 482);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textOldName);
            this.Controls.Add(this.textNewName);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textDeleteInput);
            this.Controls.Add(this.updateBtn);
            this.Controls.Add(this.readBtn);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.insertBtn);
            this.Controls.Add(this.summary_Box);
            this.Controls.Add(this.resource_TextBox);
            this.Controls.Add(this.summary);
            this.Controls.Add(this.baseClass_box);
            this.Controls.Add(this.subclass_ComboBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.armour_TextBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.hp_TextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.name_Box);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox name_Box;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox hp_TextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox armour_TextBox;
        private System.Windows.Forms.ComboBox subclass_ComboBox;
        private System.Windows.Forms.ComboBox baseClass_box;
        private System.Windows.Forms.Label summary;
        private System.Windows.Forms.TextBox resource_TextBox;
        private System.Windows.Forms.RichTextBox summary_Box;
        private System.Windows.Forms.Button insertBtn;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Button readBtn;
        private System.Windows.Forms.Button updateBtn;
        private System.Windows.Forms.TextBox textDeleteInput;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textNewName;
        private System.Windows.Forms.TextBox textOldName;
        private System.Windows.Forms.Label label10;
    }
}

